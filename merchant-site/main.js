$(document).ready(function () {
  function updateSequraPrice(price) {
    const completePrice = JSON.stringify({
      price,
      currency: 'EUR',
    });

    $('[data-sequra-host]').attr('data-price', completePrice);
  }

  updateSequraPrice($('#product-price').text());

  $('ul.menu-items > li').on('click', function () {
    $('ul.menu-items > li').removeClass('active');
    $(this).addClass('active');
  });

  $('.attr,.attr2').on('click', function () {
    var clase = $(this).attr('class');

    $('.' + clase).removeClass('active');
    $(this).addClass('active');
    $('#product-price').html($(this).attr('data-price'));

    updateSequraPrice($(this).attr('data-price'));
  });

  $('.btn-minus').on('click', function () {
    var now = $('.section > div > input').val();
    if ($.isNumeric(now)) {
      if (parseInt(now) - 1 > 0) {
        now--;
      }
      $('.section > div > input').val(now);
    } else {
      $('.section > div > input').val('1');
    }
  });

  $('.btn-plus').on('click', function () {
    var now = $('.section > div > input').val();
    if ($.isNumeric(now)) {
      $('.section > div > input').val(parseInt(now) + 1);
    } else {
      $('.section > div > input').val('1');
    }
  });
});
