import renderWidget from './js/renderWidget.js';

class Sequra {
  constructor({ sequraHost, serverUrl }) {
    // * Stores the time when the popup was opened in order to calculate the duration
    // * ex: how long does the user spent on the popup, was the content engaging enough
    this.popupOpenedAt = undefined;

    // * The price of the product in cents. ex: "12000"
    this.price = undefined;

    // * The raw price of the product.
    // * ex: {"price":"450,00 €","currency":"EUR"}
    // * used to compare if the price has changed
    // * if the price has changed
    // *     - the credit agreements are re-rendered
    // * ex: if for some reason (like UI re-render) the user set the same price in Sequra widget
    // *     - the credit agreements are not re-rendered
    this.priceRaw = undefined;

    // * The index of the selected credit agreement in the credit agreements list
    this.selectedAgreementIndex = 0;

    // * The Host DOM element of the Sequra widget
    // * ex: <div data-sequra-host data-price=""></div>
    // * this is where the Sequra widget will be rendered
    this.sequraHost = sequraHost;

    // * The URL of the Sequra server
    this.serverUrl = serverUrl;

    // * The DOM selector of the [select] element that contains the credit agreements
    this.creditAgreementsSelector = '[data-id="credit-agreements"]';

    // * The DOM selector of the [popup] element
    this.popupSelector = '[data-id="popup"]';

    // * The DOM selector of the [popup-trigger] element
    this.popupTriggerSelector = '[data-id="open-popup"]';

    // * The DOM selector of the [selected-agreement] element
    // * The [input] element inside of [Sequra widget] that contains the selected credit agreement as a value
    // * ex: <input type="hidden" data-id="selected-agreement" value="">
    this.selectedAgreementSelector = '[data-id="selected-agreement"]';

    this.getCreditAgreements = this.getCreditAgreements.bind(this);
    this.handleError = this.handleError.bind(this);
    this.init = this.init.bind(this);
    this.initDOMEventListeners = this.initDOMEventListeners.bind(this);
    this.initPriceChangeListener = this.initPriceChangeListener.bind(this);
    this.initWidget = this.initWidget.bind(this);
    this.onPopupClose = this.onPopupClose.bind(this);
    this.onPopupOpen = this.onPopupOpen.bind(this);
    this.onPriceChange = this.onPriceChange.bind(this);
    this.onSelectCreditAgreement = this.onSelectCreditAgreement.bind(this);
    this.registerEvent = this.registerEvent.bind(this);
    this.renderCreditAgreements = this.renderCreditAgreements.bind(this);
    this.setPrice = this.setPrice.bind(this);
  }

  init() {
    this.initWidget();
    this.initPriceChangeListener();
  }

  initDOMEventListeners() {
    this.sequraHost
      .querySelector(this.popupTriggerSelector)
      .addEventListener('click', this.onPopupOpen);

    this.sequraHost.querySelector(this.popupSelector).addEventListener('click', this.onPopupClose);

    this.sequraHost
      .querySelector(this.creditAgreementsSelector)
      .addEventListener('change', this.onSelectCreditAgreement);
  }

  initWidget() {
    const { dataset } = this.sequraHost;

    this.setPrice(dataset.price);
    this.renderCreditAgreements();
  }

  // * Listens to the price change event
  initPriceChangeListener() {
    try {
      const callback = (mutations) => {
        const [{ target }] = mutations;

        this.onPriceChange(target.dataset.price);
      };

      const observer = new MutationObserver(callback);
      observer.observe(this.sequraHost, { attributes: true });
    } catch (err) {
      this.handleError(err);
    }
  }

  setPrice(price) {
    try {
      const parserPrice = JSON.parse(price);
      const priceInCents = parserPrice.price.replace(/[^\d]/g, '');

      this.price = priceInCents;
      this.priceRaw = price;
    } catch (err) {
      this.handleError(err);
    }
  }

  onPriceChange(price) {
    if (this.priceRaw === price) return;

    if (!price) {
      this.handleError(
        new Error('Wrong implementation by user: "data-price" attribute is not found'),
      );

      return;
    }

    this.registerEvent({
      type: 'priceChanged',
      price,
    });

    this.setPrice(price);
    this.renderCreditAgreements();
  }

  onSelectCreditAgreement({ target }) {
    try {
      this.selectedAgreementIndex = target.selectedIndex;
      this.sequraHost.querySelector(this.selectedAgreementSelector).value = target.value;

      const { instalment_count } = JSON.parse(target.value);

      this.registerEvent({
        type: 'simulatorInstalmentChanged',
        selectedInstalment: instalment_count,
      });
    } catch (err) {
      this.handleError(err);
    }
  }

  onPopupOpen(e) {
    const popup = this.sequraHost.querySelector(this.popupSelector);

    popup.style.display = 'block';

    this.popupOpenedAt = new Date();
  }

  onPopupClose(e) {
    const { target } = e;
    const popup = this.sequraHost.querySelector(this.popupSelector);

    if (target !== popup && popup.contains(target)) return;

    popup.style.display = 'none';

    const currentDate = new Date();
    const durationInMS = currentDate - this.popupOpenedAt;

    this.registerEvent({
      type: 'moreInformationPopupViewed',
      durationInMS,
    });
  }

  async getCreditAgreements() {
    try {
      const res = await fetch(`${this.serverUrl}/credit_agreements?totalWithTax=${this.price}`);

      return res.json();
    } catch (err) {
      this.handleError(err);
    }
  }

  async renderCreditAgreements() {
    const creditAgreements = await this.getCreditAgreements();

    renderWidget({
      data: creditAgreements,
      selectedAgreementIndex: this.selectedAgreementIndex,
      sequraHost: this.sequraHost,
    });

    this.initDOMEventListeners();
  }

  registerEvent(event) {
    try {
      const body = JSON.stringify({
        context: 'checkoutWidget',
        ...event,
      });

      fetch(`${this.serverUrl}/events`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body,
      });
    } catch (err) {
      // * For the UI it's irrelevant' if the event is not registered or if the server is down
      // * All we can do is to log the error
      // TODO: register error in logs
    }
  }

  handleError(err) {
    // * Register error in telemetries
    // TODO: Depending on the error, show a message to the user
  }
}

window.addEventListener('load', async () => {
  try {
    const sequraHostSelector = `[data-sequra-host]`;
    const sequraHosts = document.querySelectorAll(sequraHostSelector);

    sequraHosts.forEach((sequraHost) => {
      const defaultProps = {
        sequraHost,
        serverUrl: 'http://localhost:8080',
      };

      const sequra = new Sequra(defaultProps);
      sequra.init();
    });
  } catch (err) {
    // * Register error in telemetries
    // TODO: Depending on the error, show a message to the user
  }
});
