const getCreditAgreements = ({ data, creditAgreementsContent: { fees, month } }) => {
  const select = document.createElement('select');
  select.dataset.id = 'credit-agreements';

  const options = data.map(({ instalment_count, instalment_total }) => {
    const option = document.createElement('option');

    const completeText = `${instalment_count} ${fees} ${instalment_total.string}/${month}`;

    option.value = JSON.stringify({ instalment_count, instalment_total: instalment_total.string });
    option.textContent = completeText;

    return option;
  });

  select.append(...options);

  return select;
};

export default getCreditAgreements;
