const getPopupHeader = ({ title, brand }) => {
  const popupHeader = document.createElement('div');
  const popupHeaderTitle = document.createElement('span');
  const popupHeaderBrand = document.createElement('div');

  popupHeader.style.display = 'flex';
  popupHeader.style.alignItems = 'center';
  popupHeader.style.justifyContent = 'space-between';
  popupHeader.style.borderBottom = '2px solid #ccc';
  popupHeaderTitle.textContent = title;
  popupHeaderBrand.textContent = brand;

  popupHeader.append(popupHeaderTitle, popupHeaderBrand);

  return popupHeader;
};

const getPopupBody = (body) => {
  const popupBody = document.createElement('div');

  const popupBodyContent = body.map(({ text, imgSrc }) => {
    const popupBodyContentItem = document.createElement('div');
    const p = document.createElement('p');
    const img = document.createElement('img');

    popupBodyContentItem.style.display = 'flex';
    popupBodyContentItem.style.alignItems = 'center';
    popupBodyContentItem.style.justifyContent = 'space-between';

    p.textContent = text;
    img.src = imgSrc;
    img.style.width = '60px';
    img.style.marginLeft = '10px';

    popupBodyContentItem.append(p, img);

    return popupBodyContentItem;
  });

  popupBody.style.padding = '20px 0';

  popupBody.append(...popupBodyContent);

  return popupBody;
};

const getPopupFooter = ({ title, text }) => {
  const popupFooter = document.createElement('div');
  const popupFooterTitle = document.createElement('span');
  const popupFooterText = document.createElement('p');

  popupFooterTitle.textContent = title;
  popupFooterTitle.style.fontWeight = 'bold';

  popupFooterText.textContent = text;
  popupFooterText.style.paddingTop = '20px';

  popupFooter.append(popupFooterTitle, popupFooterText);

  return popupFooter;
};

const getPopup = ({ header, body, footer }) => {
  const popupContent = document.createElement('div');
  const popup = document.createElement('div');

  popupContent.style.backgroundColor = '#fff';
  popupContent.style.position = 'fixed';
  popupContent.style.top = '50%';
  popupContent.style.left = '50%';
  popupContent.style.transform = 'translate(-50%, -50%)';
  popupContent.style.zIndex = '9999';
  popupContent.style.padding = '10px';

  popup.style.height = '100%';
  popup.style.width = '100%';
  popup.style.position = 'fixed';
  popup.style.top = '0';
  popup.style.left = '0';
  popup.style.zIndex = '9998';
  popup.style.display = 'none';
  popup.style.backgroundColor = 'rgba(0, 0, 0, 0.5)';
  popup.dataset.id = 'popup';

  const popupHeader = getPopupHeader(header);
  const popupBody = getPopupBody(body);
  const popupFooter = getPopupFooter(footer);

  popupContent.append(popupHeader, popupBody, popupFooter);

  popup.appendChild(popupContent);

  return popup;
};

export default getPopup;
