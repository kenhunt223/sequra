const getWidgetHeader = ({ title, actionTriggerText }) => {
  const header = document.createElement('div');
  const button = document.createElement('span');
  const label = document.createElement('p');

  header.style.display = 'flex';
  header.style.justifyContent = 'space-between';

  button.style.cursor = 'pointer';
  button.style.color = 'blue';
  button.style.textDecoration = 'underline';
  button.textContent = actionTriggerText;
  button.dataset.id = 'open-popup';

  label.textContent = title;

  header.append(label, button);

  return header;
};

export default getWidgetHeader;
