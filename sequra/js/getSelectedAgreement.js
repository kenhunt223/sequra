const getSelectedAgreement = ({ instalment_count, instalment_total }) => {
  const input = document.createElement('input');
  input.dataset.id = 'selected-agreement';
  input.type = 'hidden';
  input.value = JSON.stringify({ instalment_count, instalment_total: instalment_total.string });

  return input;
};

export default getSelectedAgreement;
