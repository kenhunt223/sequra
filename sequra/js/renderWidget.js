import getCreditAgreements from './getCreditAgreements.js';
import getWidgetHeader from './getWidgetHeader.js';
import getPopup from './getPopup.js';
import getSelectedAgreement from './getSelectedAgreement.js';

const getContent = (feePerMonth) => ({
  header: {
    title: 'Pagalo en',
    actionTriggerText: 'Mas info',
  },
  creditAgreements: {
    fees: 'cuotas en',
    month: 'mes',
  },
  popup: {
    header: {
      title: 'Fracciona tu pago',
      brand: 'SeQura',
    },
    body: [
      {
        text: '1. Eliges "Fracciona tu pago" al realizar tu pedido y pagas solo la primera cuota.',
        imgSrc: `https://pbs.twimg.com/profile_images/1126860244322344960/XU85uvGp_400x400.png`,
      },
      {
        text: '2. Recibes tu pedido.',
        imgSrc: `https://pbs.twimg.com/profile_images/1126860244322344960/XU85uvGp_400x400.png`,
      },
      {
        text: '3. El resto de pagos se cargaran automaticamente a tu targeta.',
        imgSrc: `https://pbs.twimg.com/profile_images/1126860244322344960/XU85uvGp_400x400.png`,
      },
    ],
    footer: {
      title: 'Asi de simple!',
      text: `Ademas, en el importe mostrado ya se incluye la cuota unica mensual de ${feePerMonth}/mes,
            por lo que no tendras ninguna sorpresa.`,
    },
  },
});

const getWidget = ({ data, selectedAgreementIndex, sequraHost }) => {
  const widget = document.createElement('div');

  widget.style.display = 'flex';
  widget.style.flexDirection = 'column';
  widget.style.padding = '10px';
  widget.style.border = '1px solid #ccc';
  widget.style.borderRadius = '5px';
  widget.style.width = '100%';

  const selectedAgreementData = data[selectedAgreementIndex];
  const { instalment_fee } = selectedAgreementData;

  const {
    header: headerContent,
    creditAgreements: creditAgreementsContent,
    popup: popupContent,
  } = getContent(instalment_fee.string);

  const header = getWidgetHeader(headerContent);
  const creditAgreements = getCreditAgreements({ data, creditAgreementsContent });
  const popup = getPopup(popupContent);
  const selectedAgreement = getSelectedAgreement(selectedAgreementData);

  widget.append(header, creditAgreements, popup, selectedAgreement);

  sequraHost.replaceChildren(widget);
};

export default getWidget;
