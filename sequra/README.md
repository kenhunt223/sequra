## Hi. I'm Vardan, a software engineer @S|ngular

Appreciate the opportunity.

Since we are working with widgets - I decided to create it in `vanilla JS`.

- Gives us more control
- More safety when it comes to `code-clashes`.

However I understand in production we might use more than just vanilla JS, so I also include a link to a repo with a different `test-assignment`, where I use `React`.

So you can see how `project structure`/`code` written by me looks like when using `React` or a similar tool.

```
https://gitlab.com/kenhunt223/rick_and_morty
```

### Couple of things to mention before starting

- We gonna use a placeholder image instead of icons.
- I wish I had enough time to implement [Web Components](https://developer.mozilla.org/en-US/docs/Web/Web_Components). The usage would be much better.

```html
<sequra data-sequra-host data-price="" />
```

- I will refer to the client (those who are using the Sequra widget) as `page`

---

## Widget Implementation

The page needs to import Sequra widget `index.js` file and provide a location in the DOM, where our widget is gonna be rendered.

```html
<script type="module" src="./sequra/index.js"></script>

<div data-sequra-host data-price=""></div>
```

- `data-sequra-host` attribute is the DOM selector used by our widget to find a `host` element, inside of which it will be rendered.
- `data-price` attribute should hold the `price` of the product that Sequra is financing.

```html
    <div data-sequra-host data-price="{"price":"399,99 €","currency":"EUR"}"></div>
```

- `price` attribute can contain symbols such as `.` `,` or `$`. The widget will parse them to find the `clean price in numbers`.
- `currency` attribute is not used right now (in this example it doesn't change).

### When the price on the website is changed - the `data-price` attribute should be updated with the `new price`, in a format shown above.

> NOTE: Ideally the `price` is a **_number_** (price in cents)

> ex: "{"price":"39999","currency":"EUR"}"

This could help to avoid confusions and possible bugs.

---

To get `credit agreement` information such as `how much per month` and `how many months`, after the user has selected an option.

Sequra widget provides an `input` of type `hidden` inside of the DOM `host` element, provided by page.

```html
<div data-sequra-host data-price="">
  ... ... <input data-id="selected-agreement" type="hidden"
  value="{"instalment_count":3,"instalment_total":"157,5 €"}">
</div>
```

This approach (to use an `input`) was chosen cause I think that the most common use-case to use Sequra widget is - to use it inside of a `form`

> This is just an example.

> The information inside of the `value` attribute should be extended and adjusted depending on what data exactly the `page` needs.

---

The modifications inside of the file `merchant-site/main.js` are just examples.
Some actions (ex: amount + or -) still don't update the price in the Form neither in the Sequra widget.

I didn't want to modify files outside of `sequra` folder too much.

The page should take care of how to provide/update the price inside of `data-price` attribute.

```html
    <div data-sequra-host data-price="{"price":"399,99 €","currency":"EUR"}"></div>
```

---

The page can have multiple Sequra widgets at a time. Probably with some unique selector for each element.

```html
<div data-sequra-host data-price="" data-some-unique-attr-defined-and-used-by-page-Only></div>
...
<div data-sequra-host data-price="" data-some-unique-attr-defined-and-used-by-page-Only></div>
...
<div data-sequra-host data-price="" data-some-unique-attr-defined-and-used-by-page-Only></div>
```

The only attributes required by Sequra widget are `data-sequra-host` and `data-price`.

Each widget will work within it's own context.

---

## What is missing

1. What happens when the `submit` button is clicked?

   should Sequra widget register the purchase or it is handled by the page (some POST request to Sequra servers)

2. Register event when `credit agreements` select is opened and for how long. Similar to what we have for `popup`. Could give some insights on how attractive the credit options are.

3. Register error logs.

4. I assume that the page has some agreement with Sequra. Limit of the amount that Sequra is ready to finance.

   ex: If the page is trying to sell something much more expensive than what was agreed with Sequra.

   Those checks and how does the widget handles it - are completely missing.
